# fJSON  
  
*femtoJSON* is a simple and straightforward Python 3 library, made for personal
use, that manages the JSON writing and loading, from and to Python native
dictionaries. 

The word *"femto"* in this piece of code probably refers to the number of lines
of code and not to being lightweight or something, this uses native Python
functions, no fancy C++ or anything, sorry.  
  
### But why yet another JSON library?  
I just wanted to test classes and objects in Python in an easy way. Also, if I
can write a line of code instead of 5 or 6 and have my code more organized when
the project starts to get messy, I will spend more time in the future trying to
understand it.  
  
Basically this is a reference on how to deal with JSON in Python, and a shortcut
for some of my projects to deal with them. And the repo is a template for me
on how to organize and keep a consistent design for the rest of my projects.
  
## Is this any useful?
No. Really, the overhead caused by the objects is probably not worth saving 6
lines of code. But this is fun to do.
  
## Usage
Import the library:
```python
import fJSON
```
  
Load a JSON file as a dictionary:
```python
fJSON.load(files/data.json)
```
  
Save a Python dictionary, in the same directory where you are executing your
code and with a timestamp as a name:
```python
# Creating a sample dictionary
dictionary = {"this": "is a test"}
# And saving it
fJSON.save(dictionary)
```
  
Save a Python dictionary, using a custom directory but still with a timestamp
as a name:
```python
fJSON.save(dictionary, "files/")
```
  
Save a Python dictionary, using a custom directory and a name:
```python
fJSON.save(dictionary, "files/", "myDict")
```
